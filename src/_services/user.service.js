//import config from 'config';
import { authHeader, handleResponse } from '../_helpers';
import { env } from '../App';

export const userService = {
    getAll
};

function getAll() {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`${env.baseUrl}/userData`, requestOptions)
    .then(handleResponse);
}