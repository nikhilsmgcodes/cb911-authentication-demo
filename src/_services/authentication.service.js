import { BehaviorSubject } from 'rxjs';

//import config from 'config';
import { handleResponse } from '../_helpers';
import {env} from '../App';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

export const authenticationService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue () { return currentUserSubject.value }
};

function login(phone, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ phone, password })
    };

    return fetch(`${env.baseUrl}/userLogin`, requestOptions)
        .then(handleResponse)
        .then(data => {
            if(data.error){
                alert(data.message);
                return;
            }
            else{
                localStorage.setItem('currentUser', JSON.stringify(data.data[0]));
                currentUserSubject.next(data.data[0]);
            }
            // store user details and jwt token in local storage to keep user logged in between page refreshes

            return data.data[0];
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
}
