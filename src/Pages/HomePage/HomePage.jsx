import React from 'react';

import { userService, authenticationService } from '../../_services';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: authenticationService.currentUserValue,
            users: null
        };
    }

    componentDidMount() {
        userService.getAll().then(data => {
            if(data){
                if(data.tokenExpired){
                    authenticationService.logout();
                    window.location.reload(true);
                    return;
                }
                if(data.error){
                    return alert(data.message);
                }
                else{
                    this.setState({ users: data.data })
                }
            }
        });
    }

    render() {
        const { currentUser, users } = this.state;
        return (
            <div>
                <h1>Hi {currentUser.name}!</h1>
                <p>You're logged in.</p>
                <h3>Users from secure api end point:</h3>
                {users &&
                    <ul>
                        {users.map(user =>
                            <li key={user.id}>
                            <b>Name: </b>{user.name} || <b>Email: </b>{user.email}</li>
                        )}
                    </ul>
                }
                <p>Note: User will automatically logout if no activity done by the user within 20 seconds</p>
            </div>
        );
    }
}

export { HomePage };